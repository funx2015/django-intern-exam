## Funx Backend developer test 

This is a simple software test designed to know the way how you resolve a simple problem using Python/Django.

We are looking for candidates who really enjoy coding and try to make the most reausable software elements.

## The problem

Everyday we work designing and integrating differents kind of endpoints to be consumed by massively downloaded mobile apps.

In this case you must help our client Sporting Cristal to have a copy in their own backend to serve through API to users.

## Mission:
* Database model
* Import data with a synchronizer (script) from Sporting Cristal endpoint


## Considerations
* Framework to be used: Django 2.x, Python3.x
* To do a public fork

## Sporting Cristal endpoint:

    http://futbol.funx.io/api/v2/sporting-cristal/home/match/


Good Luck!

Thanks for your time!